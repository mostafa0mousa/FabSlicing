
# FabSlicing

FabSlicing is a microservice based on a Pub/Sub design that takes an STL file and returns Gcode, an image, and a JSON containing the printing profile. The service is written in Python, Bash and uses Google Cloud Platform (GCP) to manage the Pub/Sub communication. The service can be run locally or as a Docker image. The STL file is uploaded to GCP Storage and the Pub/Sub will trigger the slicing process which generates Gcode, an image, and a JSON containing the printing profile for further processing and upload these data to GCP Storage with the taskID .

## Installation Dependencies

You must have Python 3.10.x installed.

Additionally, you'll need to include the key.json file for GCP access and create a Pub/Sub for this service using GCP console and edit it in the sub.py file.
```bash
  pip install -r requirements txt
```
install cura-engine version 1:4.13.0-1

```bash
apt install cura-engine
```


## Run Locally

```bash
  cd communication/
  python3 sub.py

```
 
## Run Docker Image 

To run you must have docker installed then mount key.json file use the code below

```bash

docker pull repo.fabminds.dev/fabminds/fabslicer-docker-image:2

```

```bash

docker run -v `pwd`/<<your-key.json-file>>:/workspaces/communication/key.json repo.fabminds.dev/fabminds/fabslicer-docker-image:2
```
