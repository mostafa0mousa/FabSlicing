FROM ubuntu:22.04

RUN apt update 

ENV CURA_ENGINE_SEARCH_PATH=/workspaces/FabMinds-Cura-Custom-Profiles/Extruder-Definitions:/workspaces/FabMinds-Cura-Custom-Profiles/Printer-Definitions \
    PATH=$CURA_ENGINE_SEARCH_PATH:$PATH

RUN set -x && apt install -y python3.10 python3.10-dev python3.10-gdbm python3-pip python3.10-distutils ffmpeg libsm6 libxext6 wget curl libgl1 libnlopt-dev git x11-apps libgl1-mesa-glx xvfb cura-engine -y


COPY . ./workspaces

WORKDIR /workspaces

RUN chmod 777 sliceModel.sh

RUN pip install -r requirements.txt

WORKDIR /workspaces/communication

ENTRYPOINT ["python3", "sub.py"]

