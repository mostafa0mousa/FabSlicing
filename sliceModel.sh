#!/bin/bash

help_function()
{
   echo ""
   echo "Usage: $0 -n model_name -s slice_setting -t task_id"
   echo -e "\t-n The name of the model to be sliced"
   echo -e "\t-s Used to override slicing settings"
   exit 1 # Exit script after printing help
}

while getopts "n:s:t" opt
do
   case "$opt" in
      n ) model_name="$OPTARG" ;;
      s ) slice_setting+=("$OPTARG") ;;
      t ) task_id="$OPTARG" ;;
      ? ) help_function ;; # Print help_function in case parameter is non-existent
   esac
done

# Assign slice_setting[0] to task_id [Bash getopts bug led me to implement it this way instead of having a seperate parameter]
task_id=${slice_setting[0]}

if [ -z "$model_name" ]
then
    echo -e "Please provide the valid name for the stl using sliceModel -n \"NAME_OF_STL\"";
    help_function

else
	
	if [ -z "$slice_setting" ]; 
	then

		 echo -e "Please provide the valid slicing settings using -s \"SETTING_VALUE\"";
    	help_function
		
	else
		
		# Invoking createSlicingProfile to override the values of fabminds.def.json, so that CuraEngine can slice properly
		# echo -e "sliceModel.sh: Slicing settings received, creating the slicing profile for $task_id..."
		python3 createSlicingProfile.py --task_id=$task_id --layer_height=${slice_setting[1]} --adhesion_type=${slice_setting[2]} --infill_sparse_density=${slice_setting[3]} --infill_pattern=${slice_setting[4]} --support_enable=${slice_setting[5]} --support_type=${slice_setting[6]} --support_z_distance=${slice_setting[7]} --wall_line_count=${slice_setting[8]} --hole_xy_offset=${slice_setting[9]} --magic_spiralize=${slice_setting[10]} 
		
		# echo -e "sliceModel.sh: JSON profile with id = $task_id has been successfully created."
		
	fi
	

	# Regex expressions to reshape our input variable model_name
	removePath=${model_name##*/} # Variable containing model_name string without the starting path
	fileName=${removePath%.*}   # Variable containing the fileName without the path or extensison
	
	# IF a directory with the fileName does not exist, then create one.
	if [ ! -d $PWD/models/ ]; then
		mkdir -p $PWD/models/
	fi

	# Invoke CuraEngine to slice the given STL and redirect the output log of the engine to engineOutput.txt file.	
	# echo -e "sliceModel.sh: Invoking the CuraEngine to slice \"$model_name\""
	CuraEngine slice -v -j $PWD/data/$task_id/slicing_profile_$task_id.def.json -l "$PWD/models/${model_name}" -o "$PWD/data/$task_id/$fileName.gcode" &> engineOutput_$task_id.txt
	# echo -e "sliceModel.sh: The model \"$model_name\" has been sliced successfully!"
	
	# Extract the correct GCode header from the log file using regular expressions aka regex and redirect the result to a txtfile
	grep -r "engineOutput_$task_id.txt" -e ";[M,L,F,T][A,a,I,L,i]:*" &> correctGCodeHeader_$task_id.txt
	
	counter=1
	while IFS= read -r line
	do
	  sed -i "$counter s/.*/$line/" "$PWD/data/$task_id/$fileName.gcode"
	  counter=$((counter+1))
	done < "correctGCodeHeader_$task_id.txt"
	
fi