from google.cloud import storage
import glob
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "key.json"

def storage_upload(storage_path, local_path, bucket_name):    
    """ Upload data to a bucket"""
    storage_client = storage.Client()

    #print(buckets = list(storage_client.list_buckets())

    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(storage_path)
    blob.upload_from_filename(local_path)
    
    #returns a public url
    return blob.public_url

def storage_download(storage_path, local_path, bucket_name):
    """ Download data from a bucket"""

    storage_client = storage.Client()
    
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(storage_path)
    

    # Download the file to a destination ex:input_vid/video.mp4
    
    blob.download_to_filename(local_path)

def storage_upload_directory(local_directory_path: str, bucket_name: str):
    
    client = storage.Client()
    rel_paths = glob.glob(local_directory_path + '/**', recursive=True)
    bucket = client.get_bucket(bucket_name)
    for local_file in rel_paths:
        remote_path = '/'.join(local_file.split('/')[3:])
        if os.path.isfile(local_file):
            
            blob = bucket.blob(remote_path)
            blob.upload_from_filename(local_file)


def create_directory(storage_directory_name, bucket_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(storage_directory_name)
    blob.upload_from_string('')


def storage_copy(source_bucket_name, source_file_name, destination_bucket_name, destination_file_name):
    """Copies a blob from one bucket to another with a new name."""
    # source_bucket_name = "your-bucket-name"
    # source_file_name = "your-object-name"
    # destination_bucket_name = "destination-bucket-name"
    # destination_file_name = "destination-object-name"

    storage_client = storage.Client()

    source_bucket = storage_client.bucket(source_bucket_name)
    source_blob = source_bucket.blob(source_file_name)
    destination_bucket = storage_client.bucket(destination_bucket_name)

    blob_copy = source_bucket.copy_blob(source_blob, destination_bucket, destination_file_name)



#samples
#upload_to_bucket("handler/README.md","/workspaces/3DReconstruct/communication/README.md","pub-sub-arch") # 1path at 

#storage_download("handler/README.md","READMEEEEE.md","pub-sub-arch")

#storage_upload_directory("2/images/cat_v2","../images/cat_v2","stl_pool_1")