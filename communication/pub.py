import argparse
import os
import sys
from google.cloud import pubsub_v1

credentials_path = 'key.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credentials_path

def pub(project_id: str, topic_id: str, data: str, attributes: dict ) -> None:
    """Publishes a message to a Pub/Sub topic."""
    # Initialize a Publisher publisher.
    publisher = pubsub_v1.PublisherClient()
    
    # Create a fully qualified identifier of form `projects/{project_id}/topics/{topic_id}`
    topic_path = publisher.topic_path(project_id, topic_id)
    
    # Data sent to Cloud Pub/Sub must be a bytestring.
    data = data.encode('utf-8') 
    
    # When you publish a message, the publisher returns a future.
    api_future = publisher.publish(topic_path, data,**attributes)
    message_id = api_future.result()

    print(f"Published {data} to {topic_path}: {message_id}")

if __name__ == "__main__":

    # Publish a message on kasai-stage FabSlicing topic with the given attributes.
    pub("kasai-stage", "FabSlicing",data="Model slicing request",attributes= {
      "task_id": "1",
      "bucket_name": "stl_pool_1",
      "stl_file_path": "cat.stl",
      "slicer_setting_path_json": "default",
      "slicer_setting_layer_height": "0.1",
      "slicer_setting_adhesion_type": "none",
      "slicer_setting_infill_sparse_density": "15",
      "slicer_setting_infill_pattern": "grid",
      "slicer_setting_support_enable": "False",
      "slicer_setting_support_type": "everywhere",
      "slicer_setting_support_z_distance": "0.0",
      "slicer_setting_wall_line_count": "3",
      "slicer_setting_hole_xy_offset": "0.0",
      "slicer_setting_magic_spiralize": "False",
    })