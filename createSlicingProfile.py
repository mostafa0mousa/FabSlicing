import sys
import getopt
import json
import os
import logging
import builtins

debug = False
print = builtins.print if debug else logging.debug

def insertSettings(path_to_json, task_id = "0", layer_height = 0.2, adhesion_type = "none", infill_sparse_density = 15, infill_pattern = "grid", support_enable = False, support_type = "everywhere", support_z_distance = 0.2, wall_line_count = 3, hole_xy_offset = 0.0, magic_spiralize = False):

    
    saving_path_json = "/workspaces/data/" + task_id + "/"
        
    # Open the FabMinds Cura printer definiton JSON file
    with open(path_to_json) as json_file:
        curaProfile = json.load(json_file)
        
    if not (curaProfile['overrides']['layer_height'] is None):
         curaProfile['overrides']['layer_height']['default_value'] = layer_height
         print("Slicing profile setting: layer_height: " + str(curaProfile['overrides']['layer_height']))
    else:
        print("Wrong key input for setting 'layer_height', review the available inputs!")
        
    if not (curaProfile['overrides']['adhesion_type'] is None):    
         curaProfile['overrides']['adhesion_type']['default_value'] = adhesion_type
         print("Slicing profile setting: adhesion_type: " + str(curaProfile['overrides']['adhesion_type']))
    else:
        print("Wrong key input for setting 'adhesion_type', review the available inputs!")
        
    if not (curaProfile['overrides']['infill_sparse_density'] is None):     
         curaProfile['overrides']['infill_sparse_density']['default_value'] = infill_sparse_density
         print("Slicing profile setting: infill_sparse_density: " + str(curaProfile['overrides']['infill_sparse_density']))
    else:
        print("Wrong key input for setting 'infill_sparse_density', review the available inputs!")
        
    if not (curaProfile['overrides']['infill_pattern'] is None):     
         curaProfile['overrides']['infill_pattern']['default_value'] = infill_pattern
         print("Slicing profile setting: infill_pattern: " + str(curaProfile['overrides']['infill_pattern']))
    else:
        print("Wrong key input for setting 'infill_pattern', review the available inputs!")
        
    if not (curaProfile.get('overrides','support_enable') is None):     
         curaProfile['overrides']['support_enable']['default_value'] = support_enable
         print("Slicing profile setting: support_enable: " + str(curaProfile['overrides']['support_enable']))
    else:
        print("Wrong key input for setting 'support_enable', review the available inputs!")
         
    if not (curaProfile['overrides']['support_type'] is None):         
         curaProfile['overrides']['support_type']['default_value'] = support_type
         print("Slicing profile setting: support_type: " + str(curaProfile['overrides']['support_type']))
    else:
        print("Wrong key input for setting 'support_type', review the available inputs!")
    
    if not (curaProfile['overrides']['support_z_distance'] is None):     
         curaProfile['overrides']['support_z_distance']['default_value'] = support_z_distance
         print("Slicing profile setting: support_z_distance: " + str(curaProfile['overrides']['support_z_distance']))
    else:
        print("Wrong key input for setting 'support_z_distance', review the available inputs!")    
    
    if not (curaProfile['overrides']['wall_line_count'] is None):     
         curaProfile['overrides']['wall_line_count']['default_value'] = wall_line_count
         print("Slicing profile setting: wall_line_count: " + str(curaProfile['overrides']['wall_line_count']))
    else:
        print("Wrong key input for setting 'wall_line_count', review the available inputs!")
        
    if not (curaProfile['overrides']['hole_xy_offset'] is None):     
         curaProfile['overrides']['hole_xy_offset']['default_value'] = hole_xy_offset
         print("Slicing profile setting: hole_xy_offset: " + str(curaProfile['overrides']['hole_xy_offset'])) 
    else:
        print("Wrong key input for setting 'hole_xy_offset', review the available inputs!")
        
    if not (curaProfile['overrides']['magic_spiralize'] is None):     
         curaProfile['overrides']['magic_spiralize']['default_value'] = magic_spiralize
         print("Slicing profile setting: magic_spiralize: " + str(curaProfile['overrides']['magic_spiralize']) + '\n')
    else:
        print("Wrong key input for setting 'magic_spiralize', review the available inputs!")
            
    # Create a directory with the unique task_id
    os.makedirs(saving_path_json, exist_ok=True)
    
    # Creating a new file named slicing_profile_x.def.json and dumping our data in it
    with open(saving_path_json + "/slicing_profile_" + task_id + ".def.json", 'w') as json_file:
        json.dump(curaProfile, json_file)
        
    return
    
def ParseBoolean (b):
    # ...
    if len(b) < 1:
        raise ValueError ('BOOLEAN ERROR: Cannot parse empty string into boolean.')
    b = b[0].lower()
    if b == 't' or b == 'y' or b == '1':
        return True
    if b == 'f' or b == 'n' or b == '0':
        return False
    raise ValueError ('BOOLEAN ERROR: Cannot parse string into boolean.')
    
def parseInputFromCMD(argv):
    
    path_stl = ""
    task_id = "0"
    path_json = "FabMinds-Cura-Custom-Profiles/Printer-Definitions/fabminds.def.json"
    layer_height = 0.2
    adhesion_type = "none"
    infill_sparse_density = 15
    infill_pattern = "grid"
    support_enable = False
    support_type = "everywhere"
    support_z_distance = 0.2
    wall_line_count = 3
    hole_xy_offset = 0.0
    magic_spiralize = False
    
    arg_help = "{0} -t <task_id input \"int\"> -l <layer_height \"float\"> -a <adhesion_type \"string\"> -b <infill_sparse_density \"int [0-100]\"> -c <infill_pattern \"string\">  -d <support_enable \"Python Boolean\">  -e <support_type \"string\"> -g <support_z_distance \"float\"> -h <wall_line_count \"int [0-4]\">  -i <hole_xy_offset \"float\"> -j <magic_spiralize \"Python Boolean>\"".format(argv[0])
    
    try:
        opts, args = getopt.getopt(argv[1:], "ht:l:a:b:c:d:e:f:g:h:i", ["help", "task_id=", "layer_height=", "adhesion_type=", "infill_sparse_density=", "infill_pattern=", "support_enable=", "support_type=", "support_z_distance=", "wall_line_count=", "hole_xy_offset=", "magic_spiralize="])
    except:
        print(arg_help)
        sys.exit(2)
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ("-t", "--task_id"):
            if(arg != "default"):
                try:
                    task_id = arg
                except:
                    task_id = "0"
        elif opt in ("-l", "--layer_height"):
            if(arg != "default"):
                try:
                    layer_height = float(arg)
                except:
                    layer_height = 0.2
        elif opt in ("-a", "--adhesion_type"):
            if(arg != "default"):
                try:
                    adhesion_type = arg
                except:
                    adhesion_type = "none"
        elif opt in ("-b", "--infill_sparse_density"):
            if(arg != "default"):
                try:
                    infill_sparse_density = int(arg)
                except:
                    infill_sparse_density = 15
        elif opt in ("-c", "--infill_pattern"):
            if(arg != "default"):
                try:
                    infill_pattern = arg
                except:
                    infill_pattern = "grid"
        elif opt in ("-d", "--support_enable"):
            if(arg != "default"):
                try:
                    support_enable = ParseBoolean(arg)
                except:
                    support_enable = False           
        elif opt in ("-e", "--support_type"):
            if(arg != "default"):
                try:
                    support_type = arg
                except:
                    support_type = "everywhere"
        elif opt in ("-g", "--support_z_distance"):
            if(arg != "default"):
                try:
                    support_z_distance = float(arg)
                except:
                    support_z_distance = 0.2
        elif opt in ("-h", "--wall_line_count"):
            if(arg != "default"):
                try:
                    wall_line_count = int(arg)
                except:
                    wall_line_count = 3
        elif opt in ("-i", "--hole_xy_offset"):
            if(arg != "default"):
                try:
                    hole_xy_offset = float(arg)
                except:
                    hole_xy_offset = 0.0
        elif opt in ("-i", "--magic_spiralize"):
            if(arg != "default"):
                try:
                    magic_spiralize = ParseBoolean(arg)
                except:
                    magic_spiralize = False
    #print('stl:', arg_stlPath)
    #print('gcode:', arg_gcodePath)
    #print('output:', arg_outputPath)
    
    return path_json, task_id, layer_height, adhesion_type, infill_sparse_density, infill_pattern, support_enable, support_type, support_z_distance, wall_line_count, hole_xy_offset, magic_spiralize 
  
if __name__ == "__main__":	   
    
    path_json, task_id, layer_height, adhesion_type, infill_sparse_density, infill_pattern, support_enable, support_type, support_z_distance, wall_line_count, hole_xy_offset, magic_spiralize = parseInputFromCMD(sys.argv)
    insertSettings(path_json, task_id = task_id, layer_height = layer_height, adhesion_type = adhesion_type, infill_sparse_density = infill_sparse_density, infill_pattern = infill_pattern, support_enable = support_enable, support_type = support_type, support_z_distance = support_z_distance, wall_line_count = wall_line_count, hole_xy_offset = hole_xy_offset, magic_spiralize = magic_spiralize)