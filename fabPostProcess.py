from PIL import Image, ImageOps
import pyvista as pv
import numpy as np
import os
import shutil
import imageio.v2 as imageio

def fabPostProcess(path_to_stl, path_to_gcode, task_id):
    pv.start_xvfb(0)
    postProcessGCode(path_to_stl, path_to_gcode, task_id)
    return

def screenshotMultiplePrespectives(path_of_stl, number_of_prespectives, background_colour, model_colour, picture_size, task_id):
    
    main_file_saving_path = "/workspaces/data/" + task_id
    # Make a temp dir to store the images in temporarily
    os.makedirs((main_file_saving_path + "/tmp_imgs"), exist_ok=True)
    
    file_head_tail = os.path.split(path_of_stl)
    images = []         
    
    for i in range(number_of_prespectives):
        mesh = pv.read(path_of_stl)
        mesh = mesh.translate((-mesh.center[0], -mesh.center[1], -mesh.center[2]), inplace=False)
        plotter = pv.Plotter(notebook=False, window_size=(int(picture_size/2),int(picture_size/2)), off_screen=True)
        model = plotter.add_mesh(mesh, color = model_colour)
	    # Plotter settings
        plotter.set_background(background_colour)
        plotter.enable_anti_aliasing('ssaa')        
        plotter.camera.zoom(1)
        plotter.center
    
        # Model orientation
        model.rotate_x(20)
        model.rotate_y(-20)
        model.rotate_z(165 + (i * int(360/number_of_prespectives) ) )
        screenshot_name = file_head_tail[1].replace(".stl","_prespective_" + str(i)) + ".png"
        plotter.screenshot(main_file_saving_path + "/tmp_imgs/" + screenshot_name)
        images.append(imageio.imread(main_file_saving_path + "/tmp_imgs/" + screenshot_name))
        
    kargs = { 'duration': 0.1 }
    # Generate a .GIF using imageio
    imageio.mimsave(main_file_saving_path + "/" + file_head_tail[1].replace(".stl",".gif"), images, **kargs)
    
    # Delete the images that were generated using plotter.screenshot (clean up)
    shutil.rmtree(main_file_saving_path + "/tmp_imgs/", ignore_errors=True)
    
    return
        
def postProcessGCode(path_of_Stl, path_of_GCode, task_id):

    main_file_saving_path = "../data/" + task_id

    # Read in the stl file using pyvista
    mesh = pv.read(path_of_Stl)
    
    mesh = mesh.translate((-mesh.center[0], -mesh.center[1], -mesh.center[2]), inplace=False)
    # Invoke a plotter object
    plotter = pv.Plotter(notebook=False, window_size=(100,100), off_screen=True)
    # Add the mesh in red to the plotter
    model = plotter.add_mesh(mesh, color = 'r')
    
    # Plotter settings
    plotter.set_background('black')
    plotter.camera.zoom(1.15)
    plotter.center

    # Model orientation
    model.rotate_x(20)
    model.rotate_y(-20)
    model.rotate_z(165)

    # Split the path into head and tail to find the file name
    file_head_tail = os.path.split(path_of_GCode)
    screenshot_name = file_head_tail[1].replace(".gcode","") + ".png"
 
    image = plotter.screenshot(main_file_saving_path + "/" + screenshot_name)
    # This is the only hardcoded path in the file, the others can be changes from main_file_saving_path
    raw_image = Image.open(r"../data/" + task_id +  "/" + screenshot_name)
    
    #screenshotMultiplePrespectives(path_of_Stl, 144, 'white', 'orange', 600, task_id)
    
    # Setting the points for cropped image
    left = 0
    top = 0
    right = 100
    bottom = 100
    # Crop then flip the image with the given dimensions
    cropped_image = raw_image.crop((left, top, right, bottom))
    flipped_image = ImageOps.flip(cropped_image)
    # Dividing the colour values by 4 to approximate their values in 18bit RGB 
    image_array = np.array(flipped_image)
    image_array = image_array/4
    image_array = image_array.astype(int)
    image_string_array = image_array.astype(str)

    # Padding any values that are not double digits with an extra 0 on the left
    for i in range(image_string_array.shape[0]):
        for j in range(image_string_array.shape[1]):
            if( int(image_string_array[i][j][0]) < 10 ):
                image_string_array[i][j][0] = '0' +  image_string_array[i][j][0]
            else:
                continue

    # Saving the image into a .txt file
    np.savetxt(file_head_tail[0] + '/' + file_head_tail[1].replace(".gcode","") + ".txt", image_string_array[::, ::, 0], header=';', delimiter='', newline="\n;",fmt="%s", comments='', footer="202100")

    def insert(file, line, column, text):
        ln, cn = line - 1, column - 1         # offset from human index to Python index
        count = 0                             # initial count of characters
        with open(file, 'r+') as f:           # open file for reading an writing
            for idx, line in enumerate(f):    # for all line in the file
                if idx < ln:                  # before the given line
                    count += len(line)        # read and count characters 
                elif idx == ln:               # once at the line                                 
                    f.seek(count + cn)        # place cursor at the correct character location
                    remainder = f.read()      # store all character afterwards                       
                    f.seek(count + cn)        # move cursor back to the correct character location
                    f.write(text + remainder) # insert text and rewrite the remainder
                    return                    # You're finished!

    # Open the stl file and insert the flipped image
    with open("/workspaces/data/" + task_id + '/' + file_head_tail[1].replace(".gcode","") + ".txt", 'r') as txtfile:
        for i in range(102):
            model_txt = txtfile.readline()
            insert(path_of_GCode, 11, 1, model_txt)

    # Remove the temp txt file generated by np.savetxt
    os.remove("/workspaces/data/" + task_id + '/' + file_head_tail[1].replace(".gcode","") + ".txt")
    return

import sys
import getopt


def parseInputFromCMD(argv):
    arg_stl_path = ""
    arg_gcode_path = ""
    arg_task_id = "0"
    arg_help = "{0} -s <stl input> -g <gcode input> -t <task_id>".format(argv[0])
    
    try:
        opts, args = getopt.getopt(argv[1:], "hs:g:t:", ["help", "stl=", 
        "gcode=", "task_id="])
    except:
        print(arg_help)
        sys.exit(2)
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ("-s", "--stl"):
            arg_stl_path = arg
        elif opt in ("-g", "--gcode"):
            arg_gcode_path = arg
        elif opt in ("-t", "--task_id"):
            arg_task_id = arg
            
    return arg_stl_path, arg_gcode_path, arg_task_id
	
if __name__ == "__main__":
    pv.start_xvfb(0)
    stl_path, gcode_path, task_id = parseInputFromCMD(sys.argv)
    postProcessGCode(stl_path, gcode_path, task_id)